var elixir = require('laravel-elixir');
elixir.config.production = true;

elixir(function (mix) {
    mix.copy('bower_components/bootstrap-sass/assets/stylesheets/', 'resources/assets/sass/')
        .copy('bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js')
        .copy('bower_components/angular/angular.min.js', 'resources/assets/js/angular.min.js')
        .copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js/jquery.min.js')
        .copy('bower_components/jquery-migrate/jquery-migrate.min.js', 'resources/assets/js/jquery-migrate.min.js')
        .scripts([
            'jquery.min.js',
            'jquery-migrate.min.js',
            'bootstrap.min.js',
            'angular.min.js'
        ])
        .sass('app.scss')
        .version(['css/app.css', 'js/all.js']);
});
